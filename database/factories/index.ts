import Factory from '@ioc:Adonis/Lucid/Factory'
import Project from 'App/Models/Project'
import Category from 'App/Models/Category'

export const ProjectFactory = Factory
  .define(Project, ({faker}) => {
    return {
      title: faker.random.word(),
      slug: faker.random.word(),
      description: faker.lorem.words(5)
    }
  }).build()

export const CategoryFactory = Factory
  .define(Category, ({faker}) => {
    return {
      title: faker.random.word(),
      slug: faker.random.word(),
      description: faker.lorem.words(5)
    }
  }).build()

export const CategoryOneFactory = Factory
  .define(Category, () => {
    return {
      title: 'Manga',
      slug: 'manga',
      description: 'Je suis une description de test'
    }
  }).build()
