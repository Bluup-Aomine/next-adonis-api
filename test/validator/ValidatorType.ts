export type ValidatorDataError = {
  rule: string,
  field: string,
  message: string
}
