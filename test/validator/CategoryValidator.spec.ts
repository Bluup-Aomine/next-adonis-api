import test from 'japa'
import supertest from 'supertest'
import Database from "@ioc:Adonis/Lucid/Database";
import {ValidatorDataError} from "./ValidatorType";

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

const getResponse = async (inputs: object | undefined = undefined) => {
  return supertest(BASE_URL)
    .post('/categories/create')
    .send(inputs)
    .expect(422);
}

test.group('Category Validator', (group) => {

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('validate error when the field title is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title must be required')
  })

  test('validate error when the field slug is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be required')
  })

  test('validate error when the field description is required', async (assert) => {
    const response = await getResponse()
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'description')

    assert.equal(error && error.message, 'The field description must be required')
  })

  test('validate error when the field title contains less than 5 characters', async (assert) => {
    const inputs = {
      title: 'test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title must be at lest 5 characters')
  })

  test('validate error when the field slug contains less than 5 characters', async (assert) => {
    const inputs = {
      slug: 'test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug must be at lest 5 characters')
  })

  test('validate error when the field description contains less than 10 characters', async (assert) => {
    const inputs = {
      description: 'test'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'description')

    assert.equal(error && error.message, 'The field description must be at lest 10 characters')
  })

  test('validate error when the field title contains more than 255 characters', async (assert) => {
    const inputs = {
      title: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'title')

    assert.equal(error && error.message, 'The field title don\'t must be at more 255 characters')
  })

  test('validate error when the field slug contains more than 255 characters', async (assert) => {
    const inputs = {
      slug: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }

    const response = await getResponse(inputs)
    const responseText = JSON.parse(response.text)

    const errors: ValidatorDataError[] = responseText.errors.errors
    const error: ValidatorDataError | undefined = errors.find(item => item.field === 'slug')

    assert.equal(error && error.message, 'The field slug don\'t must be at more 255 characters')
  })
})
