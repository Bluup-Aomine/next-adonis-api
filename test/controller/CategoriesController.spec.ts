import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
import { CategoryFactory } from 'Database/factories'
import Category from 'App/Models/Category'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('CategoryController API', (group) => {

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('fetch all categories successful', async (assert) => {
    await CategoryFactory.createMany(10)

    const response = await supertest(BASE_URL).get('/categories').expect(200)

    assert.lengthOf(response.body.categories, 10)
  })

  test('creating new category with bad credentials', async () => {
    const inputs = {
      title: '',
      slug: '',
      description: ''
    }

    await supertest(BASE_URL)
      .post('/categories/create')
      .send(inputs)
      .expect(422)
  })

  test('creating new category with good credentials', async (assert) => {
    // CODE
    const inputs = {
      title: 'Test category',
      slug: 'test-category',
      description: 'Je suis un test !'
    }

    await supertest(BASE_URL)
      .post('/categories/create')
      .send(inputs)
      .expect(200)

    const category = await Category.query()
      .orderBy('created_at', 'desc')
      .firstOrFail()
    assert.equal(category.title, inputs.title)
    assert.equal(category.slug, inputs.slug)
    assert.equal(category.description, inputs.description)
  })

})
