import test from 'japa'
import supertest from 'supertest'
import Database from '@ioc:Adonis/Lucid/Database'
import Project from 'App/Models/Project'
import {CategoryOneFactory, ProjectFactory} from 'Database/factories'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('ProjectController API', (group) => {

  group.before(async () => {
    await CategoryOneFactory.create()
  })

  group.beforeEach(async () => {
    await Database.beginGlobalTransaction()
  })

  group.afterEach(async () => {
    await Database.rollbackGlobalTransaction()
  })

  test('fetch all project successful', async (assert) => {
    await ProjectFactory.createMany(10)

    const response = await supertest(BASE_URL).get('/projects').expect(200)

    assert.lengthOf(response.body.projects, 10)
  })

  test('creating new project with bad credentials', async () => {
    const inputs = {
      title: '',
      slug: '',
      description: '',
      category: ''
    }

    await supertest(BASE_URL)
      .post('/projects/create')
      .send(inputs)
      .expect(422)
  })

  test('creating new project with bad category', async () => {
    const inputs = {
      title: 'Je suis un test',
      slug: 'je-suis-un-test',
      description: 'Description de test',
      category: 'anime'
    }

    await supertest(BASE_URL)
      .post('/projects/create')
      .send(inputs)
      .expect(422)
  })

  test('creating new project with good credentials', async (assert) => {
    // CODE
    const inputs = {
      title: 'Test project',
      slug: 'test-project',
      description: 'Je suis un test !',
      category: 'manga'
    }

    await supertest(BASE_URL)
      .post('/projects/create')
      .send(inputs)
      .expect(200)

    const project = await Project.query()
      .preload('category')
      .orderBy('created_at', 'desc')
      .firstOrFail()
    assert.equal(project.title, inputs.title)
    assert.equal(project.slug, inputs.slug)
    assert.equal(project.description, inputs.description)
    assert.equal(project.category.slug, inputs.category)
  })

})
