import { DateTime } from 'luxon'
import {BaseModel, belongsTo, BelongsTo, column} from '@ioc:Adonis/Lucid/Orm'
import Category from "App/Models/Category";

export default class Project extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({})
  public title: string

  @column({})
  public slug: string

  @column({})
  public description: string

  @column({})
  public categoryId: number

  @belongsTo(() => Category, {
    foreignKey: 'categoryId'
  })
  public category: BelongsTo<typeof Category>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
