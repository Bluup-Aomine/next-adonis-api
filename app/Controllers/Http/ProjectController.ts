import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// Validator
import ProjectValidator from 'App/Validators/ProjectValidator'
import Project from "App/Models/Project";
import Category from "App/Models/Category";

export default class ProjectController {

  /**
   * @param {HttpContextContract} {response}
   */
  public async index ({response}: HttpContextContract)
  {
      const projects = await Project.all()
      return response.status(200).send({projects})
  }

  /**
   * @param {HttpContextContract} {request, response}
   */
  public async store ({request, response}: HttpContextContract)
  {
    try {
      const projectData = await request.validate(ProjectValidator)
      // Create new project
      const project = new Project()
      project.title = projectData.title
      project.slug = projectData.slug
      project.description = projectData.description

      // Get category current
      const category = await Category.findBy('slug', projectData.category)
      if (category) await project.related('category').associate(category)
      await project.save()
    } catch (e) {
      return response.status(422).send({errors: e.messages})
    }

    return response.status(200).json({message: 'You has been create your post with successful !'})
  }
}
