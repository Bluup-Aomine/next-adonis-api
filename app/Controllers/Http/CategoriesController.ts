import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import CategoryValidator from 'App/Validators/CategoryValidator'
import Category from 'App/Models/Category'

export default class CategoriesController {

  /**
   * @param {HttpContextContract} {response}
   */
  public async index({response}: HttpContextContract) {
    const categories = await Category.all()
    return response.status(200).send({categories})
  }

  /**
   * @param {HttpContextContract} {request, response}
   */
  public async store({request, response}: HttpContextContract)
  {
    try {
      const categoryData = await request.validate(CategoryValidator)
      // Create new category
      const category = new Category()
      category.title = categoryData.title
      category.slug = categoryData.slug
      category.description = categoryData.description
      await category.save()
    } catch (e) {
      return response.status(422).send({errors: e.messages})
    }

    return response.json({message: 'You has been create your category with successful !'})
  }

}
